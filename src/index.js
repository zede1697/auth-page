import './styles/style.sass';
import Inputmask from 'inputmask';

const telInput = document.querySelector('.auth-form__phone');

const im = new Inputmask({ 
    mask: '9 ( 999 ) 999-99-99',
    showMaskOnHover: false,
    showMaskOnFocus: false,
});

const onFocus = ()=> {
    telInput.removeEventListener('keypress', onFocus);
    im.mask(telInput);
}
telInput.addEventListener('keypress', onFocus);

const submitButton = document.querySelector('.auth-form__submit');
const form = document.querySelector('.auth-form');

submitButton.setAttribute('disabled', 'disabled');
submitButton.addEventListener('click',  e => {
    const xhr = new XMLHttpRequest();
    xhr.open('POST', '#', true);
    xhr.send(new FormData(form));
    e.preventDefault();
    return false;
});

Array.prototype.slice.call(form.querySelectorAll('input')).forEach(input => {
    input.addEventListener('change', ()=> {
        if (form.checkValidity()) {
            form.setAttribute('data-complete', 'data-complete');
            submitButton.removeAttribute('disabled');
        } else {
            submitButton.setAttribute('disabled', 'disabled');
            form.removeAttribute('data-complete');
        }
    })
});


// EDGE fix
if (/Edge/.test(navigator.userAgent)) {
    [
        form.querySelector('.auth-form__pass'),
        form.querySelector('.auth-form__phone')
    ].forEach(input => {
        if (input.value.length === 0) {
            input.setAttribute('data-empty', 'data-empty');
        }
        input.addEventListener('focusout', ()=> {
            if (input.value.length === 0) {
                input.setAttribute('data-empty', 'data-empty');
            } else {
                input.removeAttribute('data-empty');
            }
        });
    })
}

const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');


const join = path.join;
const basePath = path.resolve(__dirname, '..');
const isProduction = process.env.NODE_ENV !== 'production';



module.exports = {
  entry: join(basePath, 'src', 'index.js'),
  output: {
    path: join(basePath, 'dist'),
    filename: '[name].[hash].bundle.js'
  },

  optimization: isProduction ? ({
    minimizer: [new TerserPlugin(),  new OptimizeCSSAssetsPlugin({})],
  }) : {},

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.s(a|c)ss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: process.env.NODE_ENV === 'development',
            },
          },
          "css-loader",
          "sass-loader"
        ]
      }
    ]
  },

  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: join(basePath, 'src/assets/login.html'),
      minify: {
        collapseWhitespace: true,
        removeComments: true,
        removeRedundantAttributes: true,
        removeScriptTypeAttributes: true,
        removeStyleLinkTypeAttributes: true,
        useShortDoctype: true
      },
    }),
    new CopyPlugin([
      { from: join(basePath, 'public'), to: join(basePath, 'dist') },
    ]),
    new MiniCssExtractPlugin({
      filename: "[name].[hash].css",
      chunkFilename: "[id].css",
      ignoreOrder: false,
    }),
  ],

  devServer: {
    compress: true,
    port: 9000,
    hot: true,
  },
};